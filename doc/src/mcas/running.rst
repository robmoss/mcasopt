Running MCAS for the first time
===============================

.. note:: Where to obtain MCAS from? Contact the authors via email?

.. note:: Installation requirements (libraries, etc)?

To compile MCAS, you will need to install the following:

+ The `Intel Fortran Compiler Classic (ifort) <https://www.intel.com/content/www/us/en/developer/articles/tool/oneapi-standalone-components.html#fortran>`__; and

+ The `Intel oneAPI Math Kernel Library (oneMKL) <https://www.intel.com/content/www/us/en/developer/tools/oneapi/onemkl-download.html>`__.

On Linux platforms, the default installation location is ``~/intel/oneapi``.

With the Fortran compiler and oneMKL libraries installed, you can then compile the MCAS source code:

.. code-block:: shell

   . ~/intel/oneapi/setvars.sh
   make veryclean
   make

We have successfully compiled MCAS with ``ifort`` version 2022.1.0.134 and ``oneMKL`` version 2022.1.0.223.
