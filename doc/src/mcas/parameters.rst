.. _mcas-params:

Model parameters
================

Note that the cross-section option (``xseca``) should not be enabled, because
it it does not affect the bound and resonant states, but can substantially
slow down the calculations.

.. note:: Once mcasopt_ has found a solution, you may wish to re-run MCAS with
   the cross-section option enabled.

Example parameters file
-----------------------

.. literalinclude:: ../../../src/mcasopt/example/n+12C.inp
   :caption: n+12C.inp
