Contributing to mcasopt
=======================

As an open source project, mcasopt_ welcomes contributions of many forms.

Examples of contributions include:

* Code patches
* Documentation improvements
* Bug reports and patch reviews

Licensing
---------

All contributions should be licensed under the same terms as mcasopt_ (see the
:ref:`license <License>`).

Workflow
--------

Contributions should be provided as a
`pull request <https://www.atlassian.com/git/tutorials/making-a-pull-request>`__
against the mcasopt_ master repository.
