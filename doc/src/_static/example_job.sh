#!/bin/bash
#
#SBATCH --partition=cascade
#SBATCH --time=30-00:00:00
#SBATCH --ntasks=1
#SBATCH --job-name=n+12C-job
#SBATCH --output=n+12C-job.log

module purge
module load imkl

date
cd "/home/unimelb.edu.au/rgmoss/work/projects/mcasopt/n+12C_test_mcas_runner"
"/home/unimelb.edu.au/rgmoss/work/projects/mcasopt/mcas/mcas5.4" < n+12C.inp > n+12C.out 2>&1
date
