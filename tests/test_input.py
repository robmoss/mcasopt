import os

from mcasopt.model import load_input
from mcasopt.example import save_n_plus_12C_files, remove_n_plus_12C_files


def test_input_n_plus_12C():
    """
    Check that we can load the 'n+12C' example and modify its parameters.
    """
    save_n_plus_12C_files()
    original = load_input('n+12C.inp')

    modified = original.copy()
    assert original == modified

    modified.model.targex[0].spin += 1.0
    assert original != modified

    modified_file = 'modified-n+12C.inp'
    modified.write_to(modified_file)

    new_input = load_input('modified-n+12C.inp')
    assert new_input != original
    assert new_input == modified

    remove_n_plus_12C_files()
    os.remove(modified_file)
