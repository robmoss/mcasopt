# Define a function that returns the MCAS input for parameter vector ``x``.
def input_fun(mcas_input, x):
    # Adjust the MCAS input according to the values in ``x``.
    mcas_input.model.v0 = [x[0], 0.0, x[1]]
    return mcas_input
