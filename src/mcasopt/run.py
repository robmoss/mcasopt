"""
Define the protocols and classes for running MCAS simulations on the local
machine and on HPC platforms.
"""

from dataclasses import dataclass
import importlib
import logging
import os
from pathlib import Path
import re
import shutil
import subprocess
import sys
from typing import (
    List,
    Dict,
    Any,
    Optional,
    Protocol,
    TypeVar,
    Generic,
    Callable,
    Mapping,
    Union,
    runtime_checkable,
)

import tomli

from .criteria import Criteria, parse_criteria
from .solver import Solver, minimise
from .model import Input, load_input, read_input
from .results import read_results


@dataclass
class Command:
    """
    The details required to run a command in a separate process.

    - ``args``: a list of arguments;
    - ``stdin``: optional path for reading input;
    - ``stdout``: optional path for writing output; and
    - ``cwd``: optional path to the working directory.
    """

    args: List[str]
    stdin: Optional[Path]
    stdout: Optional[Path]
    cwd: Optional[Path]


T = TypeVar('T')


@runtime_checkable
class Runner(Protocol, Generic[T]):
    """
    Provide a means of running commands.
    """

    def start_command(self, command: Command) -> T:
        """
        Start running a command, and return a handle that can be used to check
        the command's status.
        """

    def completion_status(self, handle: T) -> Optional[int]:
        """
        Return None if the job has not completed, otherwise return its exit
        status; typically, zero indicates successful completion.
        """


class McasEvaluator:
    """Evaluate MCAS outputs against a set of criteria."""

    results_file: Optional[Path]

    def __init__(
        self,
        config,
        criteria,
        mcas_input,
        runner: Runner,
        f,
        x,
        counter,
    ):
        self.config = config
        self.criteria = criteria
        self.mcas_input = mcas_input
        self.runner = runner
        self.__function = f
        self.__x = x
        self.score = None
        self.results_file = None
        self.job = None
        self.job_num = counter

    @classmethod
    def builder(cls, config, runner, function):
        """Return a function that runs MCAS simulations using the provided
        runner."""

        # TODO: validate config.experiment settings?

        criteria = config.criteria
        mcas_input = config.mcas_input()

        def evaluator(x, counter=None):
            return cls(
                config, criteria, mcas_input, runner, function, x, counter
            )

        return evaluator

    @classmethod
    def run_with_slurm(cls, config, function):
        """Return a function that runs MCAS simulations using Slurm."""
        runner = SlurmRunner(config.slurm)
        return cls.builder(config, runner, function)

    @classmethod
    def run_locally(cls, config, function):
        """
        Return a function that runs MCAS simulations on the local machine.
        """
        runner = LocalRunner()
        return cls.builder(config, runner, function)

    def command(self, job_id: str):
        """
        Return a command that defines an MCAS simulation.

        :param job_id: A unique identifier for this simulation.
        """
        # TODO: validate config.experiment and config.slurm ?

        # NOTE: use the experiment name as a prefix for the job ID.
        exp_name = self.config.experiment.name
        job_id = f'{exp_name}{job_id}'

        # Define the working directory for this specific job.
        working_dir = Path(f'./{job_id}').resolve()
        working_dir.mkdir(exist_ok=True)

        # Obtain the model input for this job.
        mcas_input = self.__function(self.mcas_input.copy(), self.__x)

        # Write the model input to the input file.
        input_file = working_dir / f'{mcas_input.fileinfo.runname}.inp'
        mcas_input.write_to(input_file)

        # Identify the output and results files.
        output_file = working_dir / f'{mcas_input.fileinfo.runname}.out'
        self.results_file = working_dir / f'{mcas_input.fileinfo.runname}.srt'

        # Delete the results file, if it exists.
        self.results_file.unlink(missing_ok=True)

        # Copy the AMDC masses data file.
        amdc_src = self.config.experiment.amdc_masses_path
        amdc_dest = working_dir / amdc_src.name
        shutil.copyfile(amdc_src, amdc_dest)

        # Retrieve the path to the MCAS binary.
        mcas_path = self.config.experiment.mcas_path

        # Use relative paths where possible.
        try:
            input_file = input_file.relative_to(working_dir)
        except ValueError:
            pass
        try:
            output_file = output_file.relative_to(working_dir)
        except ValueError:
            pass
        try:
            mcas_path = mcas_path.relative_to(working_dir)
        except ValueError:
            pass

        # Define the job command.
        cmd = Command(
            args=[mcas_path],
            stdin=input_file,
            stdout=output_file,
            cwd=working_dir,
        )
        return cmd

    def start(self, job_id: str):
        """
        Start running the MCAS simulation.

        :param job_id: A unique identifier for this simulation.
        """
        cmd = self.command(job_id)
        self.job = self.runner.start_command(cmd)

    def finished(self):
        """
        Return ``True`` if the MCAS simulation has finished.
        """
        if self.job is None:
            return False
        status = self.runner.completion_status(self.job)
        if status is None:
            return False
        elif status != 0:
            raise ValueError(f'Job failed with status {status}')
        if self.results_file.is_file():
            return self.results_file.stat().st_size > 0
        return False

    def result(self):
        """
        Return the criteria score if the MCAS simulation has finished,
        otherwise return ``None``.
        """
        if self.score is None and self.finished():
            # Evaluate the results against the criteria.
            with open(self.results_file, encoding='utf-8') as f:
                results = read_results(f)
            self.score = self.criteria.score(
                results,
                penalty_spin=self.config.experiment.penalty_spin,
                penalty_missing=self.config.experiment.penalty_missing,
            )

            # Make copies of each results file, if instructed to do so.
            valid_job_num = self.job_num is not None
            if self.config.experiment.backup_results and valid_job_num:
                parent_dir = self.results_file.parent.parent
                prefix = self.results_file.stem
                suffix = self.results_file.suffix
                dest_name = f'{prefix}_job{self.job_num:05d}{suffix}'
                dest_file = parent_dir / dest_name
                shutil.copy(self.results_file, dest_file)

            # Remove the working directory, if instructed to do so.
            if self.config.experiment.cleanup:
                working_dir = self.results_file.parent
                shutil.rmtree(working_dir)

        return self.score

    def experiment_dir(self):
        """
        Return the directory in which the MCAS simulation is running, if any.
        """
        if self.results_file is not None:
            return self.results_file.parent
        return None


class FakeMcasRunner(Runner):
    """
    Run the fake-mcas script on the local machine.
    """

    def start_command(self, command: Command):
        args = command.args
        args[0] = 'fake-mcas'
        cwd = command.cwd
        stdin = None
        # NOTE: stdin will be relative to the working directory.
        if command.stdin is not None and cwd is not None:
            stdin = open(cwd / command.stdin, 'r')
        stdout = None
        # NOTE: stdout will be relative to the working directory.
        if command.stdout is not None and cwd is not None:
            stdout = open(cwd / command.stdout, 'w')
        proc = subprocess.Popen(args, stdin=stdin, stdout=stdout, cwd=cwd)
        return proc

    def completion_status(self, popen):
        return popen.poll()


class LocalRunner(Runner):
    """
    Run MCAS on the local machine.
    """

    def start_command(self, command: Command):
        """Submit this job."""
        args = command.args
        cwd = command.cwd
        stdin = None
        # NOTE: stdin will be relative to the working directory.
        if command.stdin is not None and cwd is not None:
            stdin = open(cwd / command.stdin, 'r')
        stdout = None
        # NOTE: stdout will be relative to the working directory.
        if command.stdout is not None and cwd is not None:
            stdout = open(cwd / command.stdout, 'w')
        proc = subprocess.Popen(args, stdin=stdin, stdout=stdout, cwd=cwd)
        return proc

    def completion_status(self, popen):
        return popen.poll()


def _squeue_job_re(job_id):
    """
    Return a regular expression that searches squeue output for a job ID.

    :param job_id: The job ID to search for.
    """
    return re.compile(r'^\s*' + job_id + r'\s+', re.MULTILINE)


def _sbatch_job_re():
    """
    Return a regular expression that searches sbatch output for the job ID.
    """
    return re.compile(r'Submitted batch job (\d+)', re.MULTILINE)


class SlurmRunner:
    """
    Schedule a Slurm job and monitor its status.
    """

    def __init__(self, config):
        # TODO: validate config (Slurm) settings?
        # TODO: what other arguments do we need?
        # Working directory, job file name, command to run ...
        self.config = config
        self._squeue_re = None
        self._sbatch_re = _sbatch_job_re()

    def start_command(self, command: Command):
        """Submit this job."""
        # Write the job script in the job directory.
        job_dir = command.cwd
        if job_dir is None:
            raise ValueError('Working directory is not defined')
        job_script = job_dir / 'job.sh'
        self.write_script(job_script, command)
        # Submit the job with sbatch and retrieve the job ID.
        # NOTE: run sbatch from within the job directory.
        result = subprocess.run(
            ['sbatch', str(job_script.resolve())],
            capture_output=True,
            encoding='utf-8',
            cwd=job_dir,
        )
        if result.returncode != 0:
            raise ValueError('Could not submit job')
        re_match = self._sbatch_re.search(result.stdout)
        if re_match is None:
            raise ValueError('Could not detect submitted job ID')
        job_id = re_match.group(1)
        self._squeue_re = _squeue_job_re(job_id)
        return job_id

    def completion_status(self, job_id):
        # Check whether the job ID is listed in the user's job queue.
        result = subprocess.run(
            ['squeue', '--me'], capture_output=True, encoding='utf-8'
        )
        if result.returncode != 0:
            raise ValueError('Could not check job completion status')
        re_match = self._squeue_re.search(result.stdout)
        if re_match is not None:
            # If the job ID is found, then the job is still running.
            return None
        else:
            return 0

    def write_script(self, script_file, command: Command):
        """Write the Slurm job script to the specified file."""
        with open(script_file, 'w', encoding='utf-8') as f:
            self.write_script_to(f, command)

    def write_script_to(self, f, command: Command):
        """Write the Slurm job script to the specified file object."""
        f.write(f'{self.config.shebang}\n')
        f.write('#\n')
        for param, value in self.config.header.items():
            f.write(f'#SBATCH --{param}={value}\n')
        f.write('\n')
        f.write('module purge\n')
        for module in self.config.modules:
            f.write(f'module load {module}\n')
        f.write('\n')
        f.write('date\n')
        if command.cwd is not None:
            f.write(f'cd "{command.cwd}"\n')
        args_str = ' '.join('"' + str(arg) + '"' for arg in command.args)
        f.write(f'{args_str} < {command.stdin} > {command.stdout} 2>&1\n')
        f.write('date\n')


def _resolve_path(pathlike):
    return Path(pathlike).expanduser().resolve()


def lookup(full_name):
    """
    Retrieve an object from a fully-qualified name.

    :param str full_name: The fully-qualified name.

    :Examples:

    >>> summary_fn = lookup('mcasopt.run.load_config')
    """

    last_dot = full_name.rfind('.')
    if last_dot < 0:
        raise ValueError('No module name in "{}"'.format(full_name))
    module_name = full_name[:last_dot]
    value_name = full_name[last_dot + 1 :]

    try:
        module = importlib.import_module(module_name)
    except ModuleNotFoundError:
        logger = logging.getLogger(__name__)
        logger.debug('Could not import "{}"'.format(module_name))
        logger.debug('Trying again in the working directory ...')
        cwd = os.getcwd()
        sys.path.append(cwd)
        try:
            module = importlib.import_module(module_name)
        finally:
            sys.path.pop()
        logger.debug('Successfully imported "{}"'.format(module_name))

    # This raises AttributeError if the attribute does not exist.
    value = getattr(module, value_name)
    return value


@dataclass
class Experiment:
    """Configuration settings for a specific experiment."""

    name: str
    input_file: Path
    mcas_path: Path
    amdc_masses_path: Path
    input_function: Callable[[Input, List[float]], Input]
    x_0: List[float]
    runner: str
    backup_results: bool = False
    cleanup: bool = False
    penalty_missing: float = 1e6
    penalty_spin: float = 1e6

    def __post_init__(self):
        # Convert strings into paths, as required.
        self.input_file = _resolve_path(self.input_file)
        self.mcas_path = _resolve_path(self.mcas_path)
        self.amdc_masses_path = _resolve_path(self.amdc_masses_path)
        self.input_function = lookup(self.input_function)
        if self.runner not in ['slurm', 'local']:
            raise ValueError(f'Invalid runner: {self.runner}')
        # TODO: validate settings


@dataclass
class Slurm:
    """Configuration settings for Slurm jobs."""

    shebang: str
    header: Dict[str, str]
    modules: List[str]
    output_date: bool = True

    def __post_init__(self):
        # TODO: validate settings
        pass


@dataclass
class Config:
    """
    The configuration settings needed to run an ``mcasopt`` experiment.

    :param settings: A nested dictionary of configuration settings.
    :raise ValueError: if the settings are invalid or incomplete.
    """

    experiment: Experiment
    criteria: Criteria
    solver: Solver
    slurm: Optional[Slurm] = None

    @classmethod
    def load(cls, settings: Dict[str, Union[Any, Mapping[str, Any]]]):
        """Load configuration settings from a dictionary."""
        if 'slurm' in settings:
            slurm_args = settings.get('slurm')
            if isinstance(slurm_args, Mapping):
                slurm = Slurm(**slurm_args)
            else:
                raise ValueError(f'Invalid "slurm" settings: {slurm_args}')
        else:
            slurm = None
        return cls(
            experiment=Experiment(**settings['experiment']),
            solver=Solver(**settings['solver']),
            criteria=parse_criteria(settings.get('compound_state', [])),
            slurm=slurm,
        )

    def mcas_input(self) -> Input:
        """Load the MCAS input for this experiment."""
        return load_input(self.experiment.input_file)

    def __repr__(self):  # pragma: no cover
        kws = [f'{key}={value!r}' for key, value in self.__dict__.items()]
        return '{}({})'.format(type(self).__name__, ', '.join(kws))


def load_config(config_file) -> Config:
    """Load configuration settings from a TOML file."""
    with open(config_file, 'rb') as f:
        settings = tomli.load(f)
    return Config.load(settings)


def run_experiment(config):
    """Run the solver for the experiment defined in ``config``."""
    # Define an evaluator to run the MCAS simulations.
    input_fun = config.experiment.input_function
    if config.experiment.runner == 'slurm':
        evaluator = McasEvaluator.run_with_slurm(config, input_fun)
    elif config.experiment.runner == 'local':
        evaluator = McasEvaluator.run_locally(config, input_fun)
    else:
        raise ValueError(f'Invalid runner: {config.experiment.runner}')

    # Run the solver and obtain the approximate solution.
    result = minimise(evaluator, config.experiment.x_0, config.solver)

    # Print the solution vector.
    print(f'Solution: {result.x}')

    return result


def fake_mcas(args=None):
    """
    Provide an entry-point for running "MCAS-like" simulations that output
    perturbed results.
    This is used to test the solver without needing to run MCAS simulations.
    """
    if args is None:
        args = sys.argv[1:]

    # Read the model input from stdin and extract the input parameter vector.
    inp = read_input(sys.stdin)
    x_1 = inp.model.v0[0] + 34.16
    x_2 = inp.model.vll[0] - 0.475

    # Perturb the energy levels of the first two resonances.
    eng_1 = 0.1295 + x_1
    eng_2 = 2.0006 + x_2

    # Write the faked results to the results file.
    results_file = Path(f'{inp.fileinfo.runname}.srt')
    with open(results_file, 'w') as f:
        f.write('# energy  spin  half_width  x dx\n')
        f.write(f'{eng_1}   5/2-  3.2125e-10     1 0.5\n')
        f.write(f'{eng_2}   5/2+  1.0484e-2     1 0.5\n')
        f.write('2.6612  7/2+  9.4961e-7     1 0.5\n')
