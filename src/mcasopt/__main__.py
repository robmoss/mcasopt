"""
Run MCAS experiments with `python3 -m mcasopt config_file.toml`.
"""

import argparse
from .run import load_config, run_experiment


parser = argparse.ArgumentParser(
    prog='mcasopt', description='Run MCAS experiments'
)
parser.add_argument('config_file')
parser.add_argument('--name', help='Override the experiment name')
parser.add_argument(
    '--x0', metavar='x,y,z', help='The initial parameter vector'
)

args = parser.parse_args()
config = load_config(args.config_file)

# Override the experiment name.
if args.name:
    config.experiment.name = args.name

# Override the initial parameter vector.
if args.x0:
    try:
        config.experiment.x_0 = [float(x) for x in args.x0.split(',')]
    except ValueError as e:
        raise ValueError(f'Invalid parameter vector: {args.x0}') from e

run_experiment(config)
