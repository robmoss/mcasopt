"""A minimisation solver."""

from dataclasses import dataclass
import logging
from pathlib import Path
import subprocess
import time
from typing import (
    Callable,
    Optional,
    Protocol,
    List,
    Tuple,
    Sequence,
    Union,
    runtime_checkable,
)

import numpy as np
import scipy.optimize

from .cache import load_cache


@runtime_checkable
class Evaluator(Protocol):
    """
    A protocol for potentially-deferred evaluation of functions.
    """

    def start(self, job_id: str) -> None:
        """Begin evaluating the function."""

    def result(self) -> Optional[float]:
        """
        Return the evaluation result, or ``None`` if the evaluation has not
        finished.
        """


Builder = Callable[[np.ndarray, Optional[int]], Evaluator]
"""
A builder returns an evaluator for an input array ``x``, and an optional job
counter.
"""


def evaluate(
    evaluator: Builder,
    x: np.ndarray,
    epsilon: float,
    sleep: float,
    counter: int,
):
    """
    Evaluate f(x) at x and approximate the Jacobian.
    """
    x = np.array(x, dtype=np.float_)
    eval_x = evaluator(x, counter)
    # NOTE: we pass a unique "job ID" to each evaluation.
    eval_x.start('_x')
    while eval_x.result() is None:
        time.sleep(sleep)
    return eval_x.result()


@dataclass
class Solver:
    """Configuration settings for the optimisation solver."""

    cache_file: Path
    method: str = 'Nelder-Mead'
    bounds: Optional[List[Tuple[float, float]]] = None
    epsilon: float = 1.5e-8
    sleep_duration: float = 0.0
    log_level: str = 'INFO'

    def __post_init__(self):
        self.cache_file = Path(self.cache_file).expanduser().resolve()
        # TODO: validate settings


Vector = Sequence[Union[int, float]]


def minimise(evaluator: Builder, x0: Vector, config: Solver):
    """
    Find the approximate solution ``x`` that minimises ``f(x)``.
    """
    # methods = ['BFGS', 'Nelder-Mead', 'L-BFGS-B']

    logging.basicConfig(
        level=config.log_level,
        format='%(asctime)s:%(levelname)s:%(name)s:%(message)s',
        datefmt='%H:%M:%S',
    )
    logger = logging.getLogger(__name__)
    cache = load_cache(config.cache_file)
    counter = 0

    def target_fun(x):
        nonlocal counter
        result = cache.query(x)
        if result is None:
            logger.info(f'Evaluating at x = {x}')
            counter += 1
            result = evaluate(
                evaluator,
                x,
                epsilon=config.epsilon,
                sleep=config.sleep_duration,
                counter=counter,
            )
            logger.info(f'f(x) = {result}')
            cache.insert(x, result)
        else:
            logger.info(f'Using cached value {result} at x = {x}')

        return result

    result = scipy.optimize.minimize(
        target_fun,
        np.array(x0, dtype=np.float_),
        method=config.method,
        jac=False,
        bounds=config.bounds,
        tol=config.epsilon,
        options={
            'disp': True,
        },
        callback=lambda xk: logger.info(f'Current = {xk}'),
    )

    return result


class FunctionEvaluator:
    """
    Immediate evaluation of Python functions.
    """

    def __init__(self, f, x):
        self.__function = f
        self.__x = x
        self.__result = None

    def result(self):
        return self.__result

    def start(self, job_id):
        self.__result = self.__function(self.__x)

    @classmethod
    def builder(cls, function):
        """Return a function that evaluates Python functions."""

        def evaluator(x, counter=None):
            return cls(function, x)

        return evaluator


class ExpressionEvaluator:
    """
    Deferred evaluation of Python expressions, using subprocesses.
    """

    def __init__(self, f, x):
        self.__function = f
        self.__x = x
        self.__result = None
        self.__proc = None

    def start(self, job_id):
        expr = self.__function(self.__x)
        args = ['python3', '-c', 'print({})'.format(expr)]
        self.__proc = subprocess.Popen(args, stdout=subprocess.PIPE)

    def result(self):
        status = self.__proc.poll()
        if status is None:
            return status
        if status != 0:
            raise ValueError('Return code {}'.format(status))

        if self.__result is None:
            text = self.__proc.stdout.read()
            self.__result = float(text.strip())

        return self.__result

    @classmethod
    def builder(cls, function):
        """Return a function that evaluates Python expressions."""

        def evaluator(x, counter=None):
            return cls(function, x)

        return evaluator
