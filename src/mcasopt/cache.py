"""
Caches are used during the optimisation process to record the value of every
function evaluation, so that this can be returned in the future without having
to re-evaluate the function.
"""

import errno
import logging
import numpy as np
import pickle


class Cache(object):
    """
    Cache return values of a function.

    :param cache_file: The file in which to record the return values.
        If this file exists, the cached results will be loaded from this file.

    Instances of this class have the following member variables:

    - ``cache_file``: the file in which to record the return values; and
    - ``cache``: a dictionary that maps locations ``x`` to return values
      ``y_and_dx``.
    """

    def __init__(self, cache_file):
        self.cache_file = cache_file
        self.load_from_file()

    def load_from_file(self):
        """
        Read cached results from the cache file.
        """
        logger = logging.getLogger(__name__)
        logger.info(
            'Attempting to read cache file: {}'.format(self.cache_file)
        )
        try:
            with open(self.cache_file, 'rb') as f:
                self.cache = pickle.load(f)
            logger.info('Successfully read cache file')
            logger.info(
                'Found {} cached values for f(x)'.format(len(self.cache))
            )
        except IOError as e:
            if e.errno == errno.ENOENT:
                # File does not exist, so cache is empty.
                logger.info('Cache file does not exist')
                logger.info('Starting with an empty cache')
                self.cache = {}
            else:
                raise e

    def save_to_file(self):
        """
        Save cached results to the cache file.
        """
        logger = logging.getLogger(__name__)
        logger.debug('Saving state to cache file: {}'.format(self.cache_file))
        with open(self.cache_file, 'wb') as f:
            pickle.dump(self.cache, f)

    def contains(self, x):
        """
        Return whether the function has been evaluated at this location.

        :param x: the location at which to evaluate the function.
        """
        xl = tuple(x)
        return xl in self.cache

    def query(self, x):
        """
        Return the cached return value at a specific location, if the function
        has previously been evaluated at this location.
        Otherwise, return ``None``.

        :param x: the location at which to evaluate the function.
        """
        logger = logging.getLogger(__name__)
        xl = tuple(x)
        result = self.cache.get(xl)
        if result is None:
            logger.debug('No cached result for f(x) at x = {}'.format(x))
        else:
            logger.debug('Found cached result for f(x) at x = {}'.format(x))
        return result

    def insert(self, x, y_and_dx, save=True):
        """
        Record the value returned by the function at a new location.

        :param x: the location at which the function was evaluated.
        :param y_and_dx: the value returned by the function, which comprises
            the return value itself *and* the estimated gradient.
        :param save: whether to save the cache to disk.
        """
        logger = logging.getLogger(__name__)
        logger.debug('Recording f(x) = {} for x = {}'.format(y_and_dx, x))
        xl = tuple(x)
        self.cache[xl] = y_and_dx
        if save:
            self.save_to_file()
        min_error, param_combs = self.best_solution_to_date()
        if param_combs:
            param_list = [param for comb in param_combs for param in comb]
            param_string = ','.join(str(param) for param in param_list)
            logger.info('ITERATION:{},{}'.format(min_error, param_string))

    def best_solution_to_date(self):
        """
        Return a tuple that contains the smallest error so far, and a list
        containing the parameter values associated with that smallest error.

        If no results have been cached, this will return ``(numpy.inf, [])``.
        """
        if len(self.cache) == 0:
            return (np.inf, [])
        min_error = min(error for error in self.cache.values())
        vals = [x for (x, error) in self.cache.items() if error == min_error]
        return (min_error, vals)


def load_cache(filename):
    """
    Return a cache that records simulation results to disk.

    :param cache_file: The file in which to record the return values.
        If this file exists, the cached results will be loaded from this file.
    """
    return Cache(filename)
